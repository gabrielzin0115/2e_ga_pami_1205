import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadLivroPageRoutingModule } from './cad-livro-routing.module';

import { CadLivroPage } from './cad-livro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadLivroPageRoutingModule
  ],
  declarations: [CadLivroPage]
})
export class CadLivroPageModule {}
